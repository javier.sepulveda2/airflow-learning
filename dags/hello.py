from datetime import datetime

from airflow import DAG
from airflow.decorators import task
from airflow.operators.bash import BashOperator

once_every_5_minute_schedule = "*/5 * * * *"

additional_config = {
    "concurrent_runs": 1,
}

# A DAG represents a workflow, a collection of tasks
with DAG(dag_id="demo_2", start_date=datetime(2024, 3, 12), schedule=once_every_5_minute_schedule, params=additional_config) as dag:
    # Tasks are represented as operators
    hello = BashOperator(task_id="hello", bash_command="echo hello")

    @task()
    def airflow():
        print("airflow")

    # Set dependencies between tasks
    hello >> airflow()